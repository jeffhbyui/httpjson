import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Client {

    public static void main(String[] args){
        try {
            // SET UP PAYLOAD
            Payload payload  = new Payload();
            payload.setTitle("Higher Power");
            payload.setArtist("Coldplay");

            Server server = new Server(payload);
            String json = getContent("http://localhost/json");

            System.out.println("--Response--");
            System.out.println(json);

        } catch (IllegalArgumentException | IOException e){
            System.out.println(e);
        }
    }

    public static String getContent(String urlstring){
        String content = "";

        try {
            URL url = new URL(urlstring);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append((line + '\n'));
            }
            content = stringBuilder.toString();

        } catch (Exception e ){
            System.err.println(e.toString());
        }
        return content;
    }

    public static Payload JSONtoPayload(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Payload payload = null;
        payload = mapper.readValue(json, Payload.class);
        return payload;
    }
}
