public class Payload {

    private String body;
    private String title;

    public String getArtist() {
        return body;
    }

    public void setArtist(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
