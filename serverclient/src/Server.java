import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

public class Server {

    public Server(Payload payload) throws IOException {
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(80), 0);
        httpServer.createContext("/json", (HttpHandler) new handler(payload));
        httpServer.setExecutor(null);
        httpServer.start();
    }

    private class handler implements HttpHandler {
        private Payload _payload;

        public handler(Payload payload) {_payload = payload;}

        public void handle(HttpExchange exchange) throws IOException {
            String response = payloadToJson(_payload);
            exchange.sendResponseHeaders(200, response.length());
            exchange.getResponseHeaders().set("Context-Type", "application/json");
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    public String payloadToJson(Payload payload) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writeValueAsString(payload);

        return s;
    }



}
